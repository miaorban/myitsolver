import axios from 'axios'

export default {
  sendNewRequest (requestObj) {
    return new Promise((resolve, reject) => {
      axios.post('http://localhost:3000/incoming/new', requestObj)
        .then((response) => {
          resolve(response.data)
        })
        .catch(error => reject(error))
    })
  }
}
