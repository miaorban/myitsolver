import Vue from 'vue'
import App from 'src/App'
import VueResource from 'vue-resource'
import BootstrapVue from 'bootstrap-vue'
import router from 'src/router/router'
import helpers from '../src/library/contactApi'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEnvelope, faPhone } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faEnvelope)
library.add(faPhone)

Vue.component('font-awesome-icon', FontAwesomeIcon)

const plugin = {
  install () {
    Vue.helpers = helpers
    Vue.prototype.$helpers = helpers
  }
}

Vue.use(plugin)

Vue.use(VueResource)
Vue.use(BootstrapVue)

/* eslint-disable no-new */
new Vue({
  router,
  template: '<App/>',
  components: { App }
}).$mount('#app')
